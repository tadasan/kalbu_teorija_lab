class YuanFen
    
    #constructor
    def initialize(st, name)
        @st = st
        @name = name
        @alph = generateAlphabet
        number = generateNumber 
        @yf_number = generateYF number
    end
    
    def getYF
        return @yf_number
    end

    def generateAlphabet
        my_hash = Hash.new
        keys = ('A'..'Z').to_a
        values = (@st..@st+25).to_a
    
        my_hash =keys.zip(values).to_h

        return my_hash
    end
                 
        
    def generateNumber
        number= ""
        for i in 0..@name.length-1
            number = number + @alph[@name[i]].to_s
        end
        return number
    end
       
    def generateYF(numberStr)
        until numberStr.to_i <= 100  do
            for i in 0..numberStr.length-2
                singleNum =(numberStr[i].to_i + numberStr[i+1].to_i)%10
                numberStr[i] = singleNum.to_s
            end
            numberStr = numberStr.chop
        end
        return numberStr
    end
    
    private :generateAlphabet, :generateNumber, :generateYF

end

def getMinSt(name)
    for i in 0..10000
        first = YuanFen.new(i,name)
        yf = first.getYF
        if yf.to_i == 100 && yf.length==3
            return i
        end
    end
    return ":("
end


File.foreach( 'duom.txt' ) do |line|
    min = getMinSt line
    puts min
end





